#!/bin/bash
#SBATCH --job-name=fcn_trans
#SBATCH --output=JOB_%J.out
#SBATCH --error=JOB_%J.err
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem-per-cpu=16384
#SBATCH --constraint="gpu_vendor:nvidia"
#SBATCH --qos=long
#SBATCH --time=22:00:00
#SBATCH --mail-user=shihang@lanl.gov
#SBATCH --mail-type=ALL
#SBATCH --chdir=/projects/piml_inversion/shihang/MathGeo2020/Deep_learning/FCNVMB
#SBATCH --partition=volta-x86

python FCNVMB_train.py 
#python fcn4_mam_new_full2.py #Do your work.
